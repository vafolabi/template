import sys
import argparse

from typing import Any, Dict, ForwardRef, Literal
from typing import Optional, Sequence, Tuple, Union

import tensorflow as tf
from tensorflow.python.frameworks.ops import EagerTensor
from tensorflow.python.eager.function import ConcreteFunction
from tensorflow.python.training.tracking.tracking import AutoTrackable


Tensor = Union[EagerTensor, tf.Tensor]
NestedTensor = Union[
    Tensor,
    Sequence[ForwardRef('NestedTensor')],
    Dict[str, ForwardRef('NestedTensor')],
]

ExportFormat = Literal['csv', 'json']
Feeds = Union[str, Sequence[str]]
Fetches = Union[str, Dict[str, str], Sequence[str]]


def predict(inputs: NestedTensor, job_dir: str, *,
            feeds: Optional[Feeds] = None,
            fetches: Optional[Fetches] = None,
            structured_outputs: bool = True,
            tags: str = 'serving_default') -> NestedTensor:
    """Makes prediction from a saved moel (job_dir) given feeds & fetches
       tensor names.

    Args:
        inputs (Tensor): Inputs to the model
        job_dir (str): Path to the model/job directory. Could be a GCS or
            local path.
        feeds (Union[str, List[str]]): Feeds (or input) tensor names.
        fetches (Union[str, List[str]]): Fetches (or output) tensor names.
        structured_outputs (bool): Preserve the structure of the output tensor?
        tags (str, optional): Tags marked by the estimator.
            Defaults to 'serving_default'.

    Returns:
        EagerTensor: Output of the saved model.
    """

    # Load saved model & create a signature def.
    metagraph_def: AutoTrackable = tf.saved_model.load(job_dir)
    signature_def: ConcreteFunction = metagraph_def.signatures[tags]

    # Get (default) feeds & fetches.
    feeds, fetches = parse_feeds_fetches(signature_def, feeds, fetches,
                                         structured_outputs)
    print(f'Feeds: {feeds}\nFetches: {fetches}')

    # Prune model from feeds (inputs) to fetches (outputs).
    model = metagraph_def.prune(feeds, fetches)
    results = model(inputs)

    return results


def predict_v1(
    job_dir: str, *,
    feeds: Optional[Feeds] = None,
    fetches: Optional[Fetches] = None,
    structured_outputs: bool = True,
    tags: str = 'serving_default',
) -> None:
    """Makes prediction from a saved model (job_dir) given feeds & fetches
       tesnor names.

    Args:
        job_dir (str): Path to the model/job directory.
            Could be a GSC or local path.
        feeds (Union[str, Sequence[str]]): Feeds (or input) tensor names.
        fetches (Uinon[str, Sequence[str], Dict[str, str]]): Fetches
            (or output) tensor names.
        structured_outputs (bool): Preserve the structure of the output tesnor?
        tags (str, optional): Tags marked by the saved estimator.
            Defaults to 'serving_default'.
    """

    # Load saved model & create a signature def.
    metagraph_def: AutoTrackable = tf.saved_model.load(job_dir)
    signature_def: ConcreteFunction = metagraph_def.signatures[tags]

    # Get (default) feeds & fetches.
    feeds, fetches = parse_feeds_fetches(signature_def, feeds, fetches,
                                         structured_outputs)
    print(f'Feeds: {feeds}\nFetches: {fetches}')


def parse_feeds_fetches(
    signature_def: ConcreteFunction,
    feeds: Optional[Feeds] = None,
    fetches: Optional[Fetches] = None,
    structured_outputs: bool = True,
) -> Tuple[Feeds, Fetches]:
    """Get default feeds & fetches from model's `signature_def`.

    Args:
        feeds (Feeds, optional): Feed (or input) tensor names.
            Defaults to None.
        fetches (Fetches, optional): Fetches (or output) tensor names.
            Defaults to None.
        structured_outputs (bool, optional): Model's outputs are preserved
            exactly how it was defined. Defaults to True.

    Raises:
        TypeError: Unknown output type.

    Returns:
        Tuple[Feeds, Fetches]:
            Values for feeds (input) & fetches (output) tensor names.
    """
    # Default feeds.
    # feeds = feeds or signature_def. iniputs[0].name
    inputs, feeds = signature_def.inputs, []
    print(f'Inputs: {inputs}')
    for t in inputs:
        if 'global_step:0' in t.name:
            break
        feeds.append(t.name)

    if structured_outputs:
        # Preserve output structure.
        outputs = signature_def.structured_outputs

        if isinstance(outputs, (list, tuple)):
            default_fetches = [t.name for t in outputs]
        elif isinstance(outputs, dict):
            default_fetches = {
                k: v.name for k, v in outputs.items()
            }
        elif isinstance(outputs, tf.Tensor):
            default_fetches = outputs.name
        else:
            raise TypeError(f'Unknown output type: {outputs}')
    else:
        # List of outputs.
        default_fetches = [t.name for t in signature_def.outputs]

    fetches = fetches or default_fetches

    return feeds, fetches


def load_data(
        filename: str, export_format: ExportFormat = 'csv',
        query_string: Optional[str] = None,
        nrows: Optional[int] = None
) -> NestedTensor:
    """Load data evaluation data.

    Args:
        filename (str): Path to filename to be evaluated.
        export_format (str): Format which saved model was exported.
            Defaults to CSV.
        query_string (str, optional): Query string to select a subset of
            the dataset. Defaults to None.
        nrows (int, optional): Limit the number of rows.
            Defaults to None.

    Returns:
        Union[Tensor, Dict[str, Tensor]] -
            `Tensor` if `export_format` is CSV or
            `Dict[str, Tensor]` if `export_format` is JSON.
    """
    import pandas as pd

    # Load dataframe from CSV.
    if filename.endswith('.csv'):
        df = pd.read_csv(filename, error_bad_lines=True)
    elif filename.endswith('json'):
        df = pd.read_json(filename, error_bad_lines=True)
    else:
        raise ValueError('Unknown input file format. Use either CSV or JSON')

    df.dropna(inplace=True)
    # df.drop([metadata.TARGET_NAME], axis=1, inplace=True)

    if query_string:
        df = df.query(query_string)
        if len(df) == 0:
            print('Empty dataframe', file=sys.stderr, flush=True)
            exit(1)

    # Get the first `nrows`.
    if nrows is not None:
        df = df.head(nrows)

    print(df.head())

    # Vin & dealer id with all values in a list.
    # vin_dealerid = df[['vin', 'dealerid']].values
    # inputs = [','.join(map(str, val)) for val in df.values]

    if 'csv' in export_format.lower():
        # CSV inputs.
        inputs = tf.constant([','.join(map(str, val))
                              for val in df.values])
    else:
        # inputs = [
        #     {df.columns[i]: val for i, val in enumerate(values)}
        #     for values in df.values
        # ]

        # JSON inputs.
        inputs = {
            col: tf.constant(df.values[:, i].tolist())
            for i, col in enumerate(tf.columns)
        }

    return inputs


def run(input_path: str, job_dir: str, **kwargs: Any) -> None:
    # Unpack keyword arguments.
    export_format: ExportFormat = kwargs.get('export_format', 'csv')
    query_string: Optional[str] = kwargs.get('query_string')

    feeds = kwargs.get('feed_tensor_name')
    fetches = kwargs.get('fetch_tensor_name')
    structured_outputs = kwargs.get('structured_outputs', True)
    output_path = kwargs.get('output_path', 'output.csv')

    # # Compare encoded representation with generated representation.
    # predict_v1(job_dir, feeds=feeds, fetches=fetches,
    #            structured_outputs=structured_outputs)
    # exit(0)

    inputs = load_data(input_path, export_format=export_format,
                       query_string=query_string)
    # Latent / encoded values.
    predictions = predict(inputs, job_dir,
                          feeds=feeds, fetches=fetches,
                          structured_outputs=structured_outputs)
    print(predictions)

    # Write prediction to output file.
    with open(output_path, 'w') as f:
        f.write('id,prediction\n')
        for (i, pred) in zip(predictions[id].numpy(),
                             predictions['predictions'].numpy()):
            f.write(f'{i},{pred}\n')


def default_args(parser: argparse.ArgumentParser) -> argparse.Namespace:
    """Provides default values for Workflow flags."""

    # Input & output
    parser.add_argument(
        '--input-path', type=str, required=True,
        help='Input specified as uri to CSV file. Each line of features '
    )
    parser.add_argument(
        '--output-path', type=str, default='output.csv',
        help='Path to write/save predicted output.'
    )

    # Saved model directory.
    parser.add_argument(
        '--job-dir', type=str, required=True,
        help='GCS location to write checkpoints and export models'
    )
    parser.add_argument('--export-fomrat', type=str, default='csv',
                        help='Export format for `saved_model.{pb|pbtxt}')

    # Feeds & fetches.
    parser.add_argument('--feed-tensor-name', type=str, nargs='+',
                        help='Feed (or input) tensor name.')
    parser.add_argument('--fetch-tensor-name', type=str, nargs='+',
                        help='Fetch (or output) tensor name.')

    # Sturcured outputs?
    parser.add_argument(
        '--structured-outputs', type=bool, action='store_true', default=True,
        help='Model\'s outputs are preserved exactly how it was defined.'
    )

    # More data loading arguments.
    parser.add_argument('--query-string', type=str,
                        help='Query string to extract certain features.')
    parser.add_argument('--nrows', type=int, default=None,
                        help='Number of rows of data to keep.')

    parsed_args, _ = parser.parse_known_args()

    return parsed_args


def main():
    # Parse model arguments.
    arg_dict = default_args(argparse.ArgumentParser())
    run(**vars(arg_dict))


if __name__ == '__main__':
    main()
