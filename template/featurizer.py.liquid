#!/usr/bin/env python

# Copyright 2017 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from typing import Any, Dict, Optional

import tensorflow as tf

from {{ project }} import metadata, inputs
from {{ project }}.typing import FeatureColumn


##############################################################################
# +--------------------------------------------------------------------------+
# | YOU MAY IMPLEMENT THIS FUNCTION TO ADD EXTENDED FEATURES.
# +--------------------------------------------------------------------------+
##############################################################################
def extend_feature_columns(
        feature_columns: Dict[str, FeatureColumn],
        params: Optional[Dict[str, Any]] = None
) -> Dict[str, FeatureColumn]:
    """Use to define additional feature columns, such as bucketized_column(s),
        crossed_column(s), and embedding_column(s). task.HYPER_PARAMS can be
        used to parameterise the creation of the extended columns
        (e.g., embedding dimensions, number of buckets, etc.).

    Default behaviour is to return the original feature_columns list as-is.

    Args:
        feature_columns (Dict[str, FeatureColumn]): Mapping of feature names
            to it's respective feature columns.
        params (Dict[str, Any]): Command line arguments. Defaults to None.

    Returns:
        Dict[str, FeatrueColumn] - Extended feature columns with more feature
            columns like: embedding columns, indicator columns etc.
    """

    # A new feature column could be created here such as embedding column,
    # indicator column, one-hot, etc...

    return feature_columns


##############################################################################
# +--------------------------------------------------------------------------+
# | YOU MAY NOT CHANGE THIS FUNCTION TO CREATE FEATURE COLUMNS.
# +--------------------------------------------------------------------------+
##############################################################################
def create_feature_columns(params: Dict[str, Any]) -> Dict[str, FeatureColumn]:
    """Creates tensorFlow feature column(s) based on the metadata of the
       input features.

    The tensorFlow feature_column objects are created based on the data types
    of the features defined in the `metadata.py` module.

    The feature_column(s) are created based on the input features,
    and the constructed features (process_features method in input.py),
    during reading data files. Both type of features (input and constructed)
    should be defined in `metadata.py`.

    Extended features (if any) are created, based on the base features, as
    the extend_feature_columns method is called, before the returning complete
    the feature_column dictionary.

    Returns:
      Dict[str, Featurecolumn] - Mapping of feature name to its feature column.
          {string: tf.feature_column}: dictionary of name:feature_column .
    """

    # Load the numeric feature stats (if exists)
    feature_stats: Optional[Dict[str, Any]] = inputs.load_feature_stats()

    # All the numerical features including the input and constructed ones.
    numeric_feature_names = set(
        metadata.INPUT_NUMERIC_FEATURE_NAMES +
        metadata.CONSTRUCTED_NUMERIC_FEATURE_NAMES
    )
    seq_numeric_feature_names = set(
        metadata.SEQ_INPUT_NUMERIC_FEATURE_NAMES +
        metadata.SEQ_CONSTRUCTED_NUMERIC_FEATURE_NAMES
    )

    # Create tf.feature_column.numeric_column columns without scaling.
    if feature_stats is None:
        numeric_columns = {
            feature_name: tf.feature_column.numeric_column(
                                feature_name,
                                normalizer_fn=None
            ) for feature_name in numeric_feature_names
        }
        seq_numeric_columns = {
            feature_name: tf.feature_column.sequence_numeric_column(
                                feature_name, normalizer_fn=None
            ) for feature_name in seq_numeric_feature_names
        }

    # Create tf.feature_column.numeric_column columns with scaling.
    else:
        numeric_columns: Dict[str, Any] = {}

        for feature_name in numeric_feature_names:
            try:
                # standard scaling
                mean = feature_stats[feature_name]['mean']
                stdv = feature_stats[feature_name]['stdv']
                def normalizer_fn(x): return (x - mean) / stdv

                # max_min scaling
                # min_value = feature_stats[feature_name]['min']
                # max_value = feature_stats[feature_name]['max']
                # normalizer_fn = lambda x: (x-min_value)/(max_value-min_value)

                numeric_columns[feature_name] = \
                    tf.feature_column.numeric_column(
                        feature_name, normalizer_fn=normalizer_fn
                    )
            except Exception:
                numeric_columns[feature_name] = \
                    tf.feature_column.numeric_column(
                        feature_name, normalizer_fn=None
                    )

        seq_numeric_columns: Dict[str, Any] = {}

        for feature_name in seq_numeric_feature_names:
            try:
                # standard scaling
                mean = feature_stats[feature_name]['mean']
                stdv = feature_stats[feature_name]['stdv']
                def normalizer_fn(x): return (x - mean) / stdv

                # max_min scaling
                # min_value = feature_stats[feature_name]['min']
                # max_value = feature_stats[feature_name]['max']
                # normalizer_fn = lambda x: (x-min_value)/(max_value-min_value)

                seq_numeric_columns[feature_name] = \
                    tf.feature_column.sequence_numeric_column(
                        feature_name, normalizer_fn=normalizer_fn
                    )
            except Exception:
                seq_numeric_columns[feature_name] = \
                        tf.feature_column.sequence_numeric_column(
                            feature_name, normalizer_fn=None
                        )

    # All the categorical features with identity including the input and
    # constructed ones
    categorical_feature_names_with_identity = \
        metadata.INPUT_CATEGORICAL_FEATURE_NAMES_WITH_IDENTITY
    categorical_feature_names_with_identity.update(
        metadata.CONSTRUCTED_CATEGORICAL_FEATURE_NAMES_WITH_IDENTITY
    )
    seq_categorical_feature_names_with_identity = \
        metadata.SEQ_INPUT_CATEGORICAL_FEATURE_NAMES_WITH_IDENTITY
    seq_categorical_feature_names_with_identity.update(
        metadata.SEQ_CONSTRUCTED_CATEGORICAL_FEATURE_NAMES_WITH_IDENTITY
    )

    # Create tf.feature_column.categorical_column_with_identity columns
    categorical_columns_with_identity = {
        item[0]: tf.feature_column.categorical_column_with_identity(
                    item[0], item[1]
        ) for item in categorical_feature_names_with_identity.items()
    }
    seq_categorical_columns_with_identity = {
        item[0]: tf.feature_column.sequence_categorical_column_with_identity(
             item[0], item[1]
        ) for item in seq_categorical_feature_names_with_identity.items()
    }

    # Create tf.feature_column.categorical_column_with_vocabulary_list columns
    categorical_columns_with_vocabulary = {
        item[0]: tf.feature_column.categorical_column_with_vocabulary_list(
           item[0], item[1]
        ) for item in
        metadata.INPUT_CATEGORICAL_FEATURE_NAMES_WITH_VOCABULARY.items()
    }
    seq_categorical_columns_with_vocabulary = {
        item[0]:
        tf.feature_column.sequence_categorical_column_with_vocabulary_list(
            item[0], item[1]
        ) for item in
        metadata.SEQ_INPUT_CATEGORICAL_FEATURE_NAMES_WITH_VOCABULARY.items()
    }

    # Create tf.feature_column.categorical_column_with_vocabulary_file columns
    categorical_columns_with_vocabulary_file = {
        item[0]: tf.feature_column.categorical_column_with_vocabulary_file(
            item[0], item[1]
        ) for item in
        metadata.INPUT_CATEGORICAL_FEATURE_NAMES_WITH_VOCABULARY_FILE.items()
    }
    seq_categorical_columns_with_vocabulary_file = {
        item[0]:
        tf.feature_column.sequence_categorical_column_with_vocabulary_file(
            item[0], item[1]
        ) for item in
        metadata.SEQ_INPUT_CATEGORICAL_FEATURE_NAMES_WITH_VOCABULARY_FILE
        .items()
    }

    # Create tf.feature_column.categorical_column_with_hash_bucket columns
    categorical_columns_with_hash_bucket = {
        item[0]: tf.feature_column.categorical_column_with_hash_bucket(
            item[0], item[1]
        ) for item in
        metadata.INPUT_CATEGORICAL_FEATURE_NAMES_WITH_HASH_BUCKET.items()
    }
    seq_categorical_columns_with_hash_bucket = {
        item[0]:
        tf.feature_column.sequence_categorical_column_with_hash_bucket(
            item[0], item[1]
        ) for item in
        metadata.SEQ_INPUT_CATEGORICAL_FEATURE_NAMES_WITH_HASH_BUCKET.items()
    }

    # This will include all the feature columns of various types
    feature_columns: Dict[str, FeatureColumn] = {}

    if numeric_columns is not None:
        feature_columns.update(numeric_columns)

    if categorical_columns_with_identity is not None:
        feature_columns.update(categorical_columns_with_identity)

    if categorical_columns_with_vocabulary is not None:
        feature_columns.update(categorical_columns_with_vocabulary)

    if categorical_columns_with_vocabulary_file is not None:
        feature_columns.update(categorical_columns_with_vocabulary_file)

    if categorical_columns_with_hash_bucket is not None:
        feature_columns.update(categorical_columns_with_hash_bucket)

    if seq_numeric_columns is not None:
        feature_columns.update(seq_numeric_columns)

    if seq_categorical_columns_with_identity is not None:
        feature_columns.update(seq_categorical_columns_with_identity)

    if seq_categorical_columns_with_vocabulary is not None:
        feature_columns.update(seq_categorical_columns_with_vocabulary)

    if seq_categorical_columns_with_vocabulary_file is not None:
        feature_columns.update(seq_categorical_columns_with_vocabulary_file)

    if seq_categorical_columns_with_hash_bucket is not None:
        feature_columns.update(seq_categorical_columns_with_hash_bucket)

    # Add extended feature_column(s) before returning the complete
    # feature_column dictionary.
    return extend_feature_columns(feature_columns, params=params)
