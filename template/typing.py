# Copyright 2021 Victor I. Afolabi
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from typing import Callable
from typing import ForwardRef
from typing import Iterable
from typing import Literal
from typing import Mapping
from typing import Protocol
from typing import Union

import numpy as np
import tensorflow as tf
from tensorflow.python.feature_column.feature_column_v2 import FeatureColumn
from tensorflow.python.framework.ops import EagerTensor

__all__ = [
    'Array',
    'DataFunc',
    'FeatureColumn',
    'Mode',
    'NestedTensor',
    'NestedTensorOrArray',
    'Optimizer',
    'SupportsArray',
    'SupportsTensor',
    'Tensor',
    'TensorOrArray',
    'TensorSpec',
]

# Array-Like types.
Array = Union[np.ndarray, EagerTensor, int, float, str, bool]

# Data function.
DataFunc = Callable[[], tf.data.Dataset]

# Tensor (EagerTensor) types.
if tf.executing_eagerly():
    Tensor = Union[tf.Tensor, tf.SparseTensor, tf.RaggedTensor, EagerTensor]
else:
    Tensor = Union[tf.Tensor, tf.SparseTensor, tf.RaggedTensor]

# Tensor or Array types
TensorOrArray = Union[Tensor, Array]

# TensorSpec types.
TensorSpec = Union[
    tf.TypeSpec,
    tf.TensorSpec,
    tf.RaggedTensorSpec,
    tf.SparseTensorSpec,
]

# Placeholder type.
NestedPlaceholder = Union[
    tf.compat.v1.placeholder,
    Iterable[ForwardRef('NestedPlaceholder')],
    Mapping[str, ForwardRef('NestedPlaceholder')],
]

# NestedTensor types.
NestedTensor = Union[
    Tensor,
    Iterable[ForwardRef('NestedTensor')],
    Mapping[str, ForwardRef('NestedTensor')],
]

# Nested Tensor or Array types.
NestedTensorOrArray = Union[
    Tensor,
    Array,
    Iterable[ForwardRef('NestedTensorOrArray')],
    Mapping[str, ForwardRef('NestedTensorOrArray')],
]

# Optimizer types.
Optimizer = Union[
    tf.keras.optimizers.Optimizer,
    tf.optimizers.Optimizer,
]

# Estimator Mode types.
Mode = Literal['train', 'infer', 'eval']


class SupportsTensor(Protocol):
    def __tf_tensor__(self):
        pass


class SupportsArray(Protocol):
    def __array__(self):
        pass
