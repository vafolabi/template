"""Starting point.

Credits:

"""
import argparse

import tensorflow as tf

from {{ project }} import experiment, inputs
from {{ project }}.config import Log, FS


def initialize_hyper_params(
        parser: argparse.ArgumentParser
) -> argparse.Namespace:
    """Application's hyper-parameters and argument parser.

    Args:
        parser (argparse.ArgumentParser): Empty parser class.

    Returns:
        argparse.Namespace - Parsed arguments for both known & unknown args.
    """
    # +----------------------------------------------------------------------+
    # | Data files.
    # +----------------------------------------------------------------------+
    parser.add_argument('--train-files', required=True, nargs='+',
                        help='GCS or local paths to training data')

    parser.add_argument('--eval-files', required=True, nargs='+',
                        help='GCS or local paths to evaluation data')

    parser.add_argument('--compression-type', type=str, default=None,
                        choices=['GZIP', 'ZLIB'],
                        help='Compression type of data files')

    parser.add_argument(
        '--feature-stats-file', nargs='+', default=None,
        help='GCS or local paths to feature statistics json file'
    )

    # +----------------------------------------------------------------------+
    # | Experiment arguments -- training
    # +----------------------------------------------------------------------+
    parser.add_argument(
        '--train-steps', type=int, default=1e7,
        help=('Steps to run the training job for. If `--num-epochs` and '
              '`--train-size` are not specified, this must be. Otherwise '
              'the training job will run indefinitely. if `--num-epochs` '
              'and `--train-size` are specified, then `--train-steps` will'
              'be: `(train-size/train-batch-size) * num-epochs`.')
    )

    parser.add_argument('--train-batch-size', type=int, default=512,
                        help='Batch size for each training step')

    parser.add_argument('--train-size', type=int, default=None,
                        help='Size of training set (instance count)')

    parser.add_argument(
        '--num-epochs', type=int, default=None,
        help=('Maximum number of training data epochs on which to train.'
              'If both `--train-size` and `--num-epochs` are specified,'
              '`--train-steps` will be: `(train-size/train-batch-size) *'
              'num-epochs`.')
    )

    # +----------------------------------------------------------------------+
    # | Experiment arguments -- evaluation
    # +----------------------------------------------------------------------+
    parser.add_argument(
        '--eval-every-secs', type=int, default=120,
        help='How long to wait before running the next evaluation'
    )
    parser.add_argument(
        '--eval-steps', type=int, default=None,
        help=('Number of steps to run evaluation for at each checkpoint.'
              'Set to None to evaluate on the whole evaluation data')
    )
    parser.add_argument('--eval-batch-size', type=int, default=512,
                        help='Batch size for evaluation steps')

    # +----------------------------------------------------------------------+
    # | Saved model.
    # +----------------------------------------------------------------------+
    parser.add_argument(
        '--job-dir', type=str, required=True,
        help='GCS location to write checkpoints and export models'
    )

    parser.add_argument(
        '--reuse-job-dir', default=False, action='store_true',
        help=('Flag to decide if the model checkpoint should be re-used '
              'from the job-dir. If False then the job-dir will be '
              'deleted')
    )

    parser.add_argument(
        '--model-dir', type=str, default=FS.MODELS_DIR,
        help='GCS location to write saved model'
    )

    parser.add_argument(
        '--export-format', type=str, default='CSV',
        choices=['JSON', 'CSV', 'EXAMPLE'],
        help='The input format of the exported SavedModel binary'
    )

    parser.add_argument(
        '--runtime', type=str, default='estimator',
        choices=['estimator', 'keras'],
        help='Use either Estimator or Keras model.'
    )

    parser.add_argument('--warm-start-ckpt', type=str, default=None,
                        help='The checkpoint to warm start variables from')

    parser.add_argument('--checkpoint-path', type=str, default=FS.CKPT_DIR,
                        help='Path to the model checkpoint directory.')

    # +----------------------------------------------------------------------+
    # | Logging.
    # +----------------------------------------------------------------------+
    parser.add_argument('--verbosity', type=str, default='INFO',
                        choices=['DEBUG', 'ERROR',
                                 'FATAL', 'INFO', 'WARN'],
                        help='Verbosity & log level.')

    parser.add_argument('--seed', type=int, default=42,
                        help='Set random seed for estimator')

    parser.add_argument('--log-steps', type=int, default=10,
                        help='Log every n iteration.')

    parser.add_argument('--checkpoint-every-sec', type=int, default=120,
                        help='time between checkpoints')

    parser.add_argument('--num-checkpoint-to-save', type=int, default=3,
                        help='Number of checkpoints to save')

    # +----------------------------------------------------------------------+
    # | Model arguments.
    # +----------------------------------------------------------------------+
    parser.add_argument('--mode', type=str, default='train',
                        choices=['train', 'eval', 'infer'],
                        help='Mode to run the model.')

    parser.add_argument(
        '--num-layers', type=int, required=False, default=4,
        help=('Number of `EncoderLayer` &/or `DecoderLayer` in the'
              '`Transformer` or `EncoderTransformer` architecture.')
    )

    parser.add_argument(
        '--d-model', type=int, required=False, default=16,
        help=('Number of dimensions for the Transformer model.'
              'NOTE: `d_model` must be divisible by `num_heads`'
              'without remainder.')
    )

    parser.add_argument(
        '--num-heads', type=int, required=False, default=8,
        help=('Number of heads for `MultiHeadAttention` layer.'
              'NOTE: `d_model` must be divisible by `num_heads`'
              ' without remainder.')
    )

    parser.add_argument('--dff', type=int, default=128,
                        help=('For `point_wise_feed_forward_network`.'
                              'Feed Forword net dimension.'))

    parser.add_argument('--input-vocab-size', type=int, default=50356,
                        help='Input vocabulary size.')

    parser.add_argument(
        '--target-size', type=int, default=1,
        help=('Size of target. Size = 1 for regression model & '
              'num_classes for classification models.')
    )

    parser.add_argument('--learning-rate', type=float, default=0.01,
                        help="Learning rate value for the optimizers")

    parser.add_argument(
        '--dropout-rate', type=float, default=0.4,
        help='Rate to randomly turn off neurons during training.'
    )

    parser.add_argument('--no-use-MLS', default=True,
                        dest='use_MLS', action="store_false",
                        help="Do not use MLS layers")

    args = parser.parse_args()

    # Update model & job dirs.
    FS.JOB_DIR = args.job_dir
    FS.MODELS_DIR = args.model_dir = f'{args.job_dir.rstrip("/")}/models'
    FS.CKPT_DIR = args.checkpoint_path or f'{FS.JOB_DIR}/ckpt'

    return args


def run_experiment(args: argparse.Namespace) -> None:
    """Run experiment either to use TensorFlow estimator or Keras model."""
    if args.runtime.lower() == 'estimator':
        model = experiment.train_estimator(args)

        # Serving input function.
        serving_fn = inputs.ServingFunctionGenerator(args)
        serving_input_receiver_fn = serving_fn.get_serving_function(
            serving_function_type=args.export_format
        )

        # Export the model as a saved model.
        model.export_saved_model(
            export_dir_base=args.job_dir,
            serving_input_receiver_fn=serving_input_receiver_fn
        )
    else:
        # (Compiled) Keras model.
        model = experiment.train_keras(args)

        # Save to a TF 2.x format.
        tf.keras.models.save_model(model, args.job_dir, save_format='tf')

    Log.info(f'Model exported to: {args.job_dir}')


def main() -> int:
    # TODO: Change the epilog & description for the arg parser.
    parser = argparse.ArgumentParser(
            epilog='Text following the argument descriptions.',
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
            description='A description of what the program does.'
    )

    args: argparse.Namespace = initialize_hyper_params(parser)

    Log.info('Experiment started...')

    run_experiment(args)

    Log.info('Experiment finished...')


if __name__ == '__main__':

    main()
