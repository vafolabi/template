"""Transformer model architecture.

Credits:
    https://tensorflow.org/tutorials/text/transformer

"""
from typing import Optional, Tuple

import tensorflow as tf

from {{ project }}.network import utils
from {{ project }}.typing import Tensor


##############################################################################
# +--------------------------------------------------------------------------+
# | Multi-Head Attention
# +--------------------------------------------------------------------------+
##############################################################################
class MultiHeadAttention(tf.keras.layers.Layer):
    def __init__(self, d_model: int, num_heads: int) -> None:
        super(MultiHeadAttention, self).__init__()

        self.d_model = d_model
        self.num_heads = num_heads

        assert d_model % self.num_heads == 0, \
            '`d_model` must be divisible by `num_heads` without remainder.'

        self.depth = d_model // self.num_heads

        self.wq = tf.keras.layers.Dense(d_model)
        self.wk = tf.keras.layers.Dense(d_model)
        self.wv = tf.keras.layers.Dense(d_model)

        self.dense = tf.keras.layers.Dense(d_model)

    def split_heads(self, x: Tensor, batch_size: int) -> Tensor:
        """Split the last dimension into `(num_heads, depth)`.

        Note:
            Transpose the result such that the shape is
            `(batch_size, num_heads, seq_len, depth)`.

        Arguments:
            x {tf.Tensor} -- last dimension. Broadcastable into
                (batch_size, None, num_heads, depth)`.
            batch_size {int} -- Batch size.

        Returns:
            tf.Tensor -- Transposed result.
        """
        x = tf.reshape(x, (batch_size, -1, self.num_heads, self.depth))
        return tf.transpose(x, perm=[0, 2, 1, 3])

    def call(
            self,
            v: Tensor, k: Tensor,
            q: Tensor, mask: Optional[Tensor] = None
    ) -> Tuple[Tensor, Tensor]:
        batch_size = tf.shape(q)[0]

        q = self.wq(q)  # (batch_size, seq_len, d_model)
        k = self.wk(k)  # (batch_size, seq_len, d_model)
        v = self.wv(v)  # (batch_size, seq_len, d_model)

        # (batch_size, num_heads, seq_len_q, depth)
        q = self.split_heads(q, batch_size)
        # (batch_size, num_heads, seq_len_k, depth)
        k = self.split_heads(k, batch_size)
        # (batch_size, num_heads, seq_len_v, depth)
        v = self.split_heads(v, batch_size)

        # scaled_attn.shape == (batch_size, num_heads, seq_len_q, depth)
        # attn_weights.shape == (batch_size, num_heads, seq_len_q, seq_len_k)
        scaled_attn, attn_weights = utils.scaled_doct_product_attention(
            q, k, v, mask
        )
        # (batch_size, seq_len_q, num_heads, depth)
        scaled_attn = tf.transpose(scaled_attn, perm=[0, 2, 1, 3])

        # (batch_size, seq_len_q, d_model)
        concat_attn = tf.reshape(
            scaled_attn, shape=(batch_size, -1, self.d_model)
        )

        output = self.dense(concat_attn)  # (batch_size, seq_len_q, d_model)

        return output, attn_weights


##############################################################################
# +--------------------------------------------------------------------------+
# | Point-wise Feed Forward Network
# +--------------------------------------------------------------------------+
##############################################################################
def point_wise_feed_forward_network(d_model: int, dff: int) -> tf.keras.Model:
    net = tf.keras.Sequential()

    # (batch_size, seq_len, dff)
    net.add(tf.keras.layers.Dense(dff, activation='relu'))
    net.add(tf.keras.layers.Dense(d_model))  # (batch_size, seq_len, d_model)

    return net


##############################################################################
# +--------------------------------------------------------------------------+
# | Encoder Layer.
# +--------------------------------------------------------------------------+
##############################################################################
class EncoderLayer(tf.keras.layers.Layer):

    def __init__(
            self, d_model: int,
            num_heads: int, dff: int,
            rate: float = 0.1
    ) -> None:
        super(EncoderLayer, self).__init__()

        self.mha = MultiHeadAttention(d_model, num_heads)
        self.ffn = point_wise_feed_forward_network(d_model, dff)

        self.layer_norm1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
        self.layer_norm2 = tf.keras.layers.LayerNormalization(epsilon=1e-6)

        self.dropout1 = tf.keras.layers.Dropout(rate)
        self.dropout2 = tf.keras.layers.Dropout(rate)

    def call(self, x: Tensor, training: bool, mask: Tensor) -> Tensor:
        # (batch_size, input_seq_len, d_model)
        attn_output, _ = self.mha(x, x, x, mask)
        attn_output = self.dropout1(attn_output, training=training)
        # (batch_size, input_seq_len, d_model)
        out1 = self.layer_norm1(x + attn_output)

        ffn_output = self.ffn(out1)  # (batch_size, input_seq_len, d_model)
        ffn_output = self.dropout2(ffn_output, training=training)
        # (batch_size, input_seq_len, d_model)
        out2 = self.layer_norm2(out1 + ffn_output)

        return out2


##############################################################################
# +--------------------------------------------------------------------------+
# | Decoder Layer.
# +--------------------------------------------------------------------------+
##############################################################################
class DecoderLayer(tf.keras.layers.Layer):
    def __init__(
            self, d_model: int,
            num_heads: int, dff: int,
            rate: float = 0.1
    ) -> None:
        super(DecoderLayer, self).__init__()

        self.mha1 = MultiHeadAttention(d_model, num_heads)
        self.mha2 = MultiHeadAttention(d_model, num_heads)

        self.ffn = point_wise_feed_forward_network(d_model, dff)

        self.layer_norm1 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
        self.layer_norm2 = tf.keras.layers.LayerNormalization(epsilon=1e-6)
        self.layer_norm3 = tf.keras.layers.LayerNormalization(epsilon=1e-6)

        self.dropout1 = tf.keras.layers.Dropout(rate)
        self.dropout2 = tf.keras.layers.Dropout(rate)
        self.dropout3 = tf.keras.layers.Dropout(rate)

    def call(
            self, x: Tensor,
            enc_output: Tensor,
            training: bool,
            look_ahead_mask: Tensor,
            padding_mask: Tensor
    ) -> Tuple[Tensor, Tensor, Tensor]:
        # enc_output.shape == (batch_size, input_seq_len, d_model)

        # (batch_size, target_seq_len, d_model)
        attn1, attn_weights_block1 = self.mha1(x, x, x, look_ahead_mask)
        attn1 = self.dropout1(attn1, training=training)
        out1 = self.layer_norm1(attn1 + x)

        # (batch_size, taret_seq_len, d_model)
        attn2, attn_weights_block2 = self.mha2(enc_output, enc_output,
                                               out1, padding_mask)
        attn2 = self.dropout2(attn2, training=training)
        # (batch_size, target_seq_len, d_model)
        out2 = self.layer_norm2(attn2 + out1)

        ffn_output = self.ffn(out2)  # (batch_size, target_seq_len, d_model)
        ffn_output = self.dropout3(ffn_output, training=training)
        # (batch_size, target_seq_len, d_model)
        out3 = self.layer_norm3(ffn_output + out2)

        return out3, attn_weights_block1, attn_weights_block2


##############################################################################
# +--------------------------------------------------------------------------+
# | Encoder:
# |  1. Input Embedding
# |  2. Positional Encoding
# |  3. N encoder Layers
# +--------------------------------------------------------------------------+
##############################################################################
class Encoder(tf.keras.layers.Layer):
    def __init__(self, num_layers: int, d_model: int, num_heads: int,
                 dff: int, input_vocab_size: int, max_pos_encoding: int,
                 rate: float = 0.1) -> None:
        super(Encoder, self).__init__()

        self.d_model = d_model
        self.num_layers = num_layers

        self.embedding = tf.keras.layers.Embedding(input_vocab_size, d_model)
        self.pos_encoding = utils.positional_encoding(max_pos_encoding,
                                                      self.d_model)

        self.encoder_layers = [EncoderLayer(d_model, num_heads, dff, rate)
                               for _ in range(num_layers)]
        self.dropout = tf.keras.layers.Dropout(rate)

    def call(self, x: Tensor, training: bool = False,
             mask: Optional[Tensor] = None) -> Tuple[Tensor, Tensor]:
        seq_len = tf.shape(x)[1]

        # Adding embedding and position encoding.
        x = self.embedding(x)  # (batch_size, input_seq_len, d_model)
        embedding = x
        x *= tf.math.sqrt(tf.cast(self.d_model, tf.float32))
        x += self.pos_encoding[:, :seq_len, :]

        x = self.dropout(x, training=training)

        for i in range(self.num_layers):
            x = self.encoder_layers[i](x, training, mask)

        # (batch_size, input_seq_len, d_model)
        return x, embedding


##############################################################################
# +--------------------------------------------------------------------------+
# | Decoder
# |  1. Output Embedding
# |  2. Positional Encoding
# |  3. N decoder Layers
# +--------------------------------------------------------------------------+
##############################################################################
class Decoder(tf.keras.layers.Layer):

    def __init__(self, num_layers: int, d_model: int, num_heads: int,
                 dff: int, target_vocab_size: int, max_pos_encoding: int,
                 rate: float = 0.1) -> None:
        super(Decoder, self).__init__()

        self.d_model = d_model
        self.num_layers = num_layers

        self.embedding = tf.keras.layers.Embedding(target_vocab_size, d_model)
        self.pos_encoding = utils.positional_encoding(max_pos_encoding,
                                                      d_model)

        self.decoder_layers = [DecoderLayer(d_model, num_heads, dff, rate)
                               for _ in range(num_layers)]
        self.dropout = tf.keras.layers.Dropout(rate)

    def call(self, x: Tensor, encoder_output: Tensor, training: bool = False,
             look_ahead_mask: Optional[Tensor] = None,
             padding_mask: Optional[Tensor] = None) -> Tuple[Tensor, Tensor]:
        seq_len = tf.shape(x)[1]
        attn_weights = {}

        x = self.embedding(x)  # (batch_size, target_seq_len, d_model)
        x *= tf.math.sqrt(tf.cast(self.d_model, tf.float32))
        x += self.pos_encoding[:, :seq_len, :]

        x = self.dropout(x, training=training)

        for i in range(self.num_layers):
            x, block1, block2 = self.decoder_layers[i](
                x, encoder_output, training,
                look_ahead_mask, padding_mask
            )

            attn_weights[f'decoder_layer{i + 1}_block1'] = block1
            attn_weights[f'decoder_layer{i + 1}_block2'] = block2

        # x.shape == (batch_size, target_seq_len, d_model)
        return x, attn_weights


##############################################################################
# +--------------------------------------------------------------------------+
# | Transformer.
# +--------------------------------------------------------------------------+
# ############################################################################
class Transformer(tf.keras.Model):
    def __init__(self, num_layers: int, d_model: int, num_heads: int,
                 dff: int, input_vocab_size: int, target_vocab_size: int,
                 pe_input: int, pe_target: int, rate: float = 0.1) -> None:
        super(Transformer, self).__init__()

        self.encoder = Encoder(num_layers, d_model, num_heads,
                               dff, input_vocab_size, pe_input, rate)
        self.decoder = Decoder(num_layers, d_model, num_heads,
                               dff, target_vocab_size, pe_target, rate)
        self.final_layer = tf.keras.layers.Dense(target_vocab_size)

    def call(
            self, inputs: Tensor,
            targets: Optional[Tensor] = None,
            training: bool = False,
            encoder_padding_mask: Optional[Tensor] = None,
            lookahead_mask: Optional[Tensor] = None,
            decoder_padding_mask: Optional[Tensor] = None
    ) -> Tuple[Tensor, Tensor]:

        # encoder_output.shape == (batch_size, input_seq_len, d_model)
        encoder_output = self.encoder(inputs, training, encoder_padding_mask)

        # decoder_output.shape == (batch_size, target_seq_len, d_model)
        decoder_output, attn_weights = self.decoder(targets, encoder_output,
                                                    training, lookahead_mask,
                                                    decoder_padding_mask)
        # final_output.shape == (batch_size, target_seq_len, target_vocab_size)
        final_output = self.findal_layer(decoder_output)

        return final_output, attn_weights


class EncoderTransformer(tf.keras.Model):
    def __init__(self, num_layers: int, d_model: int, num_heads: int,
                 dff: int, input_vocab_size: int, target_size: int,
                 pe_input: int, rate: float = 0.1) -> None:
        super(EncoderTransformer, self).__init__()

        self.encoder = Encoder(num_layers, d_model, num_heads,
                               dff, input_vocab_size, pe_input, rate)
        self.flatten = tf.keras.layers.Flatten()

        self.dense1 = tf.keras.layers.Dense(input_vocab_size)
        self.dense2 = tf.keras.layers.Dense(128)
        self.final_layer = tf.keras.layers.Dense(target_size,
                                                 activation='linear')

    def call(
            self, inputs: Tensor, training: bool = False,
            encoder_padding_mask: Optional[Tensor] = None
    ) -> Tensor:
        """Call the model on input features / feature Tensors.

        Args:
            inputs (tf.Tensor): A rank-2 tensor: (batch_size, input_seq_len).
            training (bool): Specify if we're on training mode or not.
                Defaults to False.
            encoder_padding_mask (tf.Tensor, optional): Encoder padding mask.
                Defaults to None.

        Returns:
            Tuple[tf.Tensor, tf.Tensor]: An output tensor representing TARGET,
                and input embedding tensor.
        """

        # encoder_output.shape == (batch_size, input_seq_len, d_model)
        # encoder_output=(?, 16, 16)  encoder_embedding=(?, 16, 16)
        encoder_output, _ = self.encoder(
            inputs, training, encoder_padding_mask
        )
        # Log.warn(f'Encoder output: {encoder_output}')
        # Log.warn(f'Encoder embedding: {encoder_embedding}')

        # flatten.shape == (?, 256)
        flatten = self.flatten(encoder_output)
        # Log.warn(f'Flatten: {flatten}')

        # dense1.shape == (?, 50_356)
        dense1 = self.dense1(flatten)
        # Log.warn(f'Dense1: {dense1}')

        # dense2.shape == (?, 128)
        dense2 = self.dense2(dense1)
        # Log.warn(f'Dense2: {dense2}')

        # output.shape == (?, 1)
        output = self.final_layer(dense2)
        # Log.warn(f'Output: {output}')

        # return output
        return output
