import tensorflow as tf


class PartialRestoreHook(tf.estimator.SessionRunHook):

    def __init__(self, warm_start_ckpt: str) -> None:
        self.saver: tf.compat.v1.train.Saver = None
        self.ckpt_path: str = tf.train.latest_checkpoint(warm_start_ckpt)

    def begin(self) -> None:
        # Create a new checkpoint reader.
        reader = tf.compat.v1.train.NewCheckpointReader(self.ckpt_path)
        saved_shapes = reader.get_variable_to_shape_map()

        # Get all vairables name.
        var_names = [
            (var.name, var.name.split(':')[0])
            for var in tf.compat.v1.global_variables()
            if var.name.split(':')[0] in saved_shapes
            and 'global_step' != var.name.split(':')[0]
            and 'Adagrad' != var.name.split(':')[0].split('/')[-1]
        ]

        # Name and variables mapping.
        name2var = dict(zip(
            map(lambda x: x.name.split(':')[0],
                tf.compat.v1.global_variables()),
            tf.compat.v1.global_variables()
        ))

        # Get restore variables.
        restore_vars = []
        with tf.variable_scope('', reuse=True):
            for _, saved_var_name in var_names:
                var = name2var[saved_var_name]
                var_shape = var.get_shape().as_list()
                if var_shape == saved_shapes[saved_var_name]:
                    restore_vars.append(var)

        # Save restore variables.
        tf.logging.info(f'Optimistic restore vars: {restore_vars}')
        self.saver = tf.compat.v1.train.Saver(restore_vars)

    def after_create_session(
            self,
            session: tf.compat.v1.Session,
            coord
    ) -> None:
        # Restore variables.
        self.saver.restore(session, self.ckpt_path)
