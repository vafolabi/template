#!/usr/bin/env python

# Copyright 2017 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from typing import Dict, List, Optional, Union
##############################################################################
# +--------------------------------------------------------------------------+
# | YOU NEED TO MODIFY THE FOLLOWING METADATA TO ADAPT THE TRAINER TEMPLATE
# | TO YOUR DATA.
# +--------------------------------------------------------------------------+
##############################################################################

# TODO: YOU NEED TO MODIFY THE FOLLOWING METADATA TO ADAPT THE TRAINER TEMPLATE
# TO YOUR DATA.

# A List of all the columns (header) present in the input data
# file(s) in order to parse it.
# Note that, not all the columns present here will be input
# features to your model.
# If header represents a sequence the header name should be made a
# pair in the form
# ('HEADER',sequence length).
HEADER: List[str] = []

# List of the default values of all the columns present in the input data.
# This helps decoding the data types of the columns.
HEADER_DEFAULTS: List[Union[int, float, str]] = []

# List of the feature names of type int or float.
INPUT_NUMERIC_FEATURE_NAMES: List[str] = []

# Numeric features constructed, if any, in process_features
# function in input.py module, as part of reading data.
CONSTRUCTED_NUMERIC_FEATURE_NAMES: List[str] = []

# Dictionary of feature names with int values, but to be treated
# as categorical features.
# In the dictionary, the key is the feature name, and the value is the
# num_buckets (count of distinct values).
INPUT_CATEGORICAL_FEATURE_NAMES_WITH_IDENTITY: Dict[str, int] = {}

# Categorical features with identity constructed, if any, in process_features
# function in input.py module, as part of reading data. Usually include
# constructed boolean flags.
CONSTRUCTED_CATEGORICAL_FEATURE_NAMES_WITH_IDENTITY: Dict[str, int] = {}

# Dictionary of categorical features with few nominal values
# (to be encoded as one-hot indicators).
# In the dictionary, the key is the feature name, and the value is the
# list of feature vocabulary.
INPUT_CATEGORICAL_FEATURE_NAMES_WITH_VOCABULARY: Dict[str, List[str]] = {}

# Dictionary of categorical features with few nominal values
# (to be encoded as one-hot indicators).
# In the dictionary, the key is the feature name, and the value is the
# list of feature vocabulary.
INPUT_CATEGORICAL_FEATURE_NAMES_WITH_VOCABULARY_FILE: Dict[str, str] = {}

# Dictionary of categorical features with many values (sparse features).
# In the dictionary, the key is the feature name, and the value is the
# bucket size.
INPUT_CATEGORICAL_FEATURE_NAMES_WITH_HASH_BUCKET: Dict[str, int] = {}

# List of the feature names of type int or float.
SEQ_INPUT_NUMERIC_FEATURE_NAMES: List[str] = []

# Numeric features constructed, if any, in process_features function in
# input.py module, as part of reading data.
SEQ_CONSTRUCTED_NUMERIC_FEATURE_NAMES: List[str] = []

# Dictionary of feature names with int values, but to be treated as
# categorical features.
# In the dictionary, the key is the feature name, and the value is the
# num_buckets (count of distinct values).
SEQ_INPUT_CATEGORICAL_FEATURE_NAMES_WITH_IDENTITY: Dict[str, int] = {}

# Categorical features with identity constructed, if any, in process_features
# function in input.py module, as part of reading data. Usually include
# constructed boolean flags.
SEQ_CONSTRUCTED_CATEGORICAL_FEATURE_NAMES_WITH_IDENTITY: Dict[str, int] = {}

# Dictionary of categorical features with few nominal values
# (to be encoded as one-hot indicators).
# In the dictionary, the key is the feature name, and the value is the
# list of feature vocabulary.
SEQ_INPUT_CATEGORICAL_FEATURE_NAMES_WITH_VOCABULARY: Dict[str, List[str]] = {}

# Dictionary of categorical features with few nominal values
# (to be encoded as one-hot indicators).
# In the dictionary, the key is the feature name, and the value is the
# list of feature vocabulary.
SEQ_INPUT_CATEGORICAL_FEATURE_NAMES_WITH_VOCABULARY_FILE: Dict[str, str] = {}

# Dictionary of categorical features with many values (sparse features).
# In the dictionary, the key is the feature name, and the value is the
# bucket size.
SEQ_INPUT_CATEGORICAL_FEATURE_NAMES_WITH_HASH_BUCKET: Dict[str, int] = {}

# List of all the categorical feature names.
# This is programmatically created based on the previous inputs.
INPUT_CATEGORICAL_FEATURE_NAMES: List[str] = \
        list(INPUT_CATEGORICAL_FEATURE_NAMES_WITH_IDENTITY.keys()) + \
        list(INPUT_CATEGORICAL_FEATURE_NAMES_WITH_VOCABULARY.keys()) + \
        list(INPUT_CATEGORICAL_FEATURE_NAMES_WITH_VOCABULARY_FILE.keys()) + \
        list(INPUT_CATEGORICAL_FEATURE_NAMES_WITH_HASH_BUCKET.keys())

SEQ_INPUT_CATEGORICAL_FEATURE_NAMES = \
    list(SEQ_INPUT_CATEGORICAL_FEATURE_NAMES_WITH_IDENTITY.keys()) + \
    list(SEQ_INPUT_CATEGORICAL_FEATURE_NAMES_WITH_VOCABULARY.keys()) + \
    list(SEQ_INPUT_CATEGORICAL_FEATURE_NAMES_WITH_VOCABULARY_FILE.keys()) + \
    list(SEQ_INPUT_CATEGORICAL_FEATURE_NAMES_WITH_HASH_BUCKET.keys())

# List of all the input feature names to be used in the model.
# This is programmatically created based on the previous inputs.
INPUT_FEATURE_NAMES: List[str] = INPUT_NUMERIC_FEATURE_NAMES + \
    INPUT_CATEGORICAL_FEATURE_NAMES + \
    SEQ_INPUT_NUMERIC_FEATURE_NAMES + \
    SEQ_INPUT_CATEGORICAL_FEATURE_NAMES

# Column includes the relative weight of each record.
WEIGHT_COLUMN_NAME: Optional[str] = None

# Target feature name (response or class variable).
TARGET_NAME: str = ''

# List of the class values (labels) in a classification dataset.
TARGET_LABELS: List[str] = []

# List of the columns expected during serving (which is probably different
# to the header of the training data).
SERVING_COLUMNS: List[str] = []

# List of the default values of all the columns of the serving data.
# This helps decoding the data types of the columns.
SERVING_DEFAULTS: List[Union[int, float, str]] = []

# List of the columns expected during serving (which is probably different
# to the header of the training data).
JSON_SERVING_COLUMNS: List[str] = []

# List of the default values of all the columns of the serving data.
# This helps decoding the data types of the columns.
JSON_SERVING_DEFAULTS: List[Union[int, float, str]] = []
