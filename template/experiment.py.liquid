import argparse
from typing import Any, Dict, Tuple

import tensorflow as tf

from {{ project }}.config import Log
from {{ project }}.typing import DataFunc, Tensor
from {{ project }} import featurizer, inputs, metadata, model


def load_data(args: argparse.Namespace) -> Tuple[DataFunc, DataFunc]:
    """Load training and evaluation dataset.

    Args:
        args (argparse.Namespace): Command line argument.

    Return:
        Tuple[
            Callable[[], tf.data.Dataset],  # Training data
            Callable[[], tf.data.Dataset],  # Evaluation data.
        ] - Returns input functions for both training and evaluation data.
    """
    # Train input function.
    train_input_fn = inputs.generate_input_fn(
        file_names_pattern=args.train_files,
        compression_type=args.compression_type,
        mode=tf.estimator.ModeKeys.TRAIN,
        num_epochs=args.num_epochs,
        batch_size=args.train_batch_size,
        batch_by_context=args.use_MLS,
    )

    # Evaluation input function.
    eval_input_fn = inputs.generate_input_fn(
        file_names_pattern=args.eval_files,
        compression_type=args.compression_type,
        mode=tf.estimator.ModeKeys.EVAL,
        batch_size=args.eval_batch_size,
        batch_by_context=args.use_MLS,
    )

    return train_input_fn, eval_input_fn


def feature_column(
        features: tf.data.Dataset,
        labels: tf.data.Dataset,
        params: Dict[str, Any]
   ) -> Tuple[Tensor, Tensor]:
    """Load feature columns from dataset.

    Args:
        features (tf.data.Dataset): Features from dataset.
        labels (tf.data.Dataset): Labels from dataset.
        params (Dict[str, Any]): Command line arguments converted to dicts.

    Returns:
        Tuple[Tensor, Tensor] - Returns features and labels passed through
            the loaded feature columns.
    """
    # Load feature columns.
    feature_columns = featurizer.create_feature_columns(params)

    # TODO: Input features for dense layers.
    # List of feature columns.
    columns = [feature_columns['...'], ...]

    # # Pass the feature columns into Sequence feature layer.
    # feature_layer = tf.keras.experimental.SequenceFeatures(
    #     columns, trainable=True, name=None,
    # )
    # # Parse the feature column layers with the dataset features.
    # feature_inputs, feature_len = feature_layer(
    #     features=features, training=None
    # )
    # feature_len_mask = tf.sequence_mask(feature_inputs)

    # Pass the feature columns into a Dense feature layer.
    feature_layer = tf.keras.layers.DenseFeatures(columns)
    feature_inputs = feature_layer(features)

    Log.debug(f'Feature layer: {feature_layer}')
    Log.debug(f'Feature input: {feature_inputs}')

    # # For Sequence layers.
    # Log.debug(f'Feature Length: {feature_len}')
    # Log.debug(f'Feature Length Mask: {feature_len_mask}')

    # Target Dense layer.
    target_layer = tf.keras.layers.DenseFeatures(
            feature_columns[metadata.TARGET_NAME]
    )
    target_inputs = target_layer(labels)

    return feature_inputs, target_inputs


def train_keras(args: argparse.Namespace) -> tf.keras.Model:
    """Build and training Keras models.

    Args:
        args (argparse.Namespace): Command line argument.

    Returns:
        tf.keras.Model - Trained Keras model.
    """
    # Load train & eval input function.
    train_input_fn, eval_input_fn = load_data(args)
    train_data, eval_data = train_input_fn(), eval_input_fn()

    # Compute the number of training steps based on num_epoch, train_size,
    # and train_batch_size
    if args.train_size is not None and args.num_epochs is not None:
        train_steps = int((args.train_size / args.train_batch_size)
                          * args.num_epochs)
    else:
        train_steps = args.train_steps

    # Train iterator/feature columns.
    train_iter = iter(train_data)
    feature_train, labels_train = next(train_iter)
    train_fc = feature_column(feature_train, labels_train, params=vars(args))

    # Eval iterator/feature columns.
    eval_iter = iter(eval_data)
    feature_eval, labels_eval = next(eval_iter)
    eval_fc = feature_column(feature_eval, labels_eval, params=vars(args))

    # Setup Learning Rate decay.
    lr_decay_cb = tf.keras.callbacks.LearningRateScheduler(
        lambda epoch: args.learning_rate + 0.02 * (0.5 ** (1 + epoch)),
        verbose=True
    )

    # Setup TensorBoard callbacks.
    tensorboard_cb = tf.keras.callbacks.TensorBoard(
        args.job_dir, histogram_freq=1
    )

    # (Compiled) Keras model.
    keras_model = model.create_keras(args)

    # Train model.
    keras_model.fit(
        # train_fc,
        x=train_fc[0],
        y=train_fc[1],
        steps_per_epoch=train_steps,
        epochs=args.num_epochs,
        validation_data=eval_fc,
        validation_steps=1, verbose=1,
        callbacks=[lr_decay_cb, tensorboard_cb]
    )

    return keras_model


def train_estimator(args: argparse.Namespace) -> tf.estimator.Estimator:
    """Build, train and evaluate keras Estimator.

    Args:
        args (argparse.Namespace): Command line argument.

    Return:
        tf.estimator.Estimator - Trained Tensorflow Estimator.
    """

    # Load train & eval input functions.
    train_input_fn, eval_input_fn = load_data(args)

    # Compute the number of training steps based on num_epoch, train_size,
    # and train_batch_size
    if args.train_size is not None and args.num_epochs is not None:
        train_steps = int((args.train_size / args.train_batch_size)
                          * args.num_epochs)
    else:
        train_steps = args.train_steps

    # Serving input function.
    serving_fn = inputs.ServingFunctionGenerator(args)
    serving_input_receiver_fn = serving_fn.get_serving_function(
        serving_function_type=args.export_format
    )
    exporter = tf.estimator.FinalExporter(
        'estimator',
        serving_input_receiver_fn,
        as_text=True
    )

    # Train input spec.
    train_spec = tf.estimator.TrainSpec(
        train_input_fn,
        max_steps=train_steps
    )

    # Eval input spec.
    eval_spec = tf.estimator.EvalSpec(
        eval_input_fn,
        exporters=[exporter],
        throttle_secs=args.eval_every_secs,
    )

    # Estimator run configuration.
    run_config = tf.estimator.RunConfig(
        model_dir=args.job_dir,
        tf_random_seed=args.seed,
        log_step_count_steps=args.log_steps,
        # change if you want to change frequency of saving checkpoints
        save_checkpoints_secs=args.checkpoint_every_sec,
        keep_checkpoint_max=args.num_checkpoint_to_save,
    )

    # Create TF estimator from model_fn.
    estimator = tf.estimator.Estimator(
        model_fn=model.model_fn,
        model_dir=args.job_dir,
        config=run_config,
        params=vars(args),
        warm_start_from=args.warm_start_ckpt
    )

    # Train & evaluate estimator.
    tf.estimator.train_and_evaluate(estimator, train_spec, eval_spec)

    return estimator
