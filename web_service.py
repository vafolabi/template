import argparse

from typing import Any, Dict, ForwardRef, List, Literal
from typing import Optional, Sequence, Tuple, Union

import tornado.web
import tornado.ioloop

import numpy as np
import tensorflow as tf

from tensorflow.python.frameworks.ops import EagerTensor
from tensorflow.python.eager.function import ConcreteFunction
from tensorflow.python.training.tracking.tracking import AutoTrackable


# Type hints.
Array = Union[np.ndarray, int, float, bytes, str, bool]
Tensor = Union[EagerTensor, tf.Tensor]
NestedTensor = Union[
    Tensor,
    Sequence[ForwardRef('NestedTensor')],
    Dict[str, ForwardRef('NestedTensor')],
]
NestedArray = Union[
    Array,
    Sequence[ForwardRef('NestedArray')],
    Dict[str, ForwardRef('NestedArray')],
]

ExportFormat = Literal['csv', 'json']
Feeds = Union[str, Sequence[str]]
Fetches = Union[str, Dict[str, str], Sequence[str]]


##############################################################################
# +--------------------------------------------------------------------------+
# | Tornado Main Handler.
# +--------------------------------------------------------------------------+
##############################################################################
class MainHandler(tornado.web.RequestHandler):

    def initialize(self,
                   job_dir: str,
                   feeds: Optional[Feeds] = None,
                   fetches: Optional[Fetches] = None,
                   structured_outputs: bool = True) -> None:

        self.job_dir: str = job_dir

        # Load saved model & create a signature def.
        self.metagraph_def: AutoTrackable = tf.saved_model.load(self.job_dir)
        self.signature_def: ConcreteFunction = \
            self.metagraph_def.signatures['serving_default']

        # Get (default) feeds & fetches.
        feeds, fetches = self.parse_feeds_fetches(feeds, fetches,
                                                  structured_outputs)

        # Prune model from feeds (inputs) to fetches (outputs).
        self.model = self.metagraph_def.prune(feeds, fetches)

    def parse_feeds_fetches(
            self, feeds: Optional[Feeds] = None,
            fetches: Optional[Fetches] = None,
            structured_outputs: bool = True
    ) -> Tuple[Feeds, Fetches]:
        """Get default feeds & fetches from model's `signature_def`.

        Args:
            feeds (Union[str, Sequence[str]], optional): Feed (or input)
                tensor names. Defaults to None.
            fetches (Union[str, Sequence[str], Dict[str, str]], optional):
                Fetches (or output) tensor names. Defaults to None.
            structured_outputs (bool, optional): Model's outputs are preserved
                exactly how it was defined. Defaults to True.

        Raises:
            TypeError: Unknown output type.

        Returns:
            Tuple[Feeds, Fetches]: Values for feeds (input) & fetches (output)
                tensor names.
        """
        # Default feeds.
        # feeds = feeds or self.signature_def.inputs[0].name
        inputs, feeds = self.signature_def.inputs, []
        print(f'Inputs: {inputs}')
        for t in inputs:
            if 'global_step:0' in t.name:
                break
            feeds.append(t.name)

        if structured_outputs:
            # Preserve output structure.
            outputs = self.signature_def.structured_outputs

            if isinstance(outputs, (list, tuple)):
                default_fetches = [t.name for t in outputs]
            elif isinstance(outputs, dict):
                default_fetches = {
                    k: v.name for k, v in outputs.items()
                }
            elif isinstance(outputs, tf.Tensor):
                default_fetches = outputs.name
            else:
                raise TypeError(f'Unknown output type: {outputs}')
        else:
            # List of outputs.
            default_fetches = [t.name for t in self.signature_def.outputs]

        fetches = fetches or default_fetches

        return feeds, fetches

    def get(self) -> None:
        # Input data.
        data = tornado.escape.json_decode(self.request.body)
        print(f'{data=}')

        # Make predictions.
        predictions = self.model(tf.constant(data))
        print(f'RAW PRED: {predictions}')

        predictions = self.custom_process(predictions)
        print(f'PROCESSED PRED: {predictions}')

        # predictions = self.postprocess(predictions)
        # print(f'PROCESSED PRED: {predictions}')

        # Encode predictions into Tornado response
        response = tornado.escape.json_encode({"predictions": predictions})
        self.write(response)

    def custom_process(
        self, predictions: NestedTensor
        # ) -> NestedArray:
    ) -> List[Tuple[int, float]]:
        """A Custom method to process output of the model's predictions."""
        ids = predictions['id'].numpy().tolist()
        preds = predictions['prediction'].numpy().tolist()

        return list(zip(ids, preds))

    def postprocess(self, predictions: NestedTensor) -> NestedArray:
        def decode_byte_values(array: Array) -> NestedArray:
            decoded_array = []
            for value in array:
                if isinstance(value, bytes):
                    decoded_array.append(value.decode('utf-8'))
                    continue
                elif isinstance(value, np.ndarray):
                    if len(value) == 1:
                        decoded_array.append(value.item())
                        continue
                    else:
                        decoded_array.append(decode_byte_values(value))
                        continue
                elif isinstance(value, np.generic):
                    decoded_array.append(value.item())
                else:
                    decoded_array.append(value)
            return decoded_array

        return [
            dict(zip(predictions, decode_byte_values(t)))
            for t in zip(*predictions.values())
        ]


def make_app(args: argparse.Namespace) -> tornado.web.Application:

    handlers = [(r'/', MainHandler,
                 dict(job_dir=args.job_dir,
                      feeds=args.feed_tensor_name,
                      fetches=args.fetch_tensor_name,
                      structured_outputs=args.structured_outputs))]
    return tornado.web.Application(handlers, debug=True)


def default_args() -> argparse.Namespace:
    """Provides default values for Workflow flags."""
    parser: argparse.ArgumentParser = argparse.ArgumentParser()

    parser.add_argument(
        '--job-dir', type=str, required=True,
        help='GCS location to write checkpoints and export models'
    )

    # Feed & fetch tensor names.
    parser.add_argument('--feed-tensor-name', type=str, nargs='+',
                        help='Feed (or input) tensor names.')
    parser.add_argument('--fetch-tensor-name', type=str, nargs='+',
                        help='Fetch (or output) tensor names.')

    parser.add_argument(
        '--structured-outputs', type=bool, action='store_true', default=True,
        help='Model\'s outputs are preserved exactly how it was defined.'
    )

    parser.add_argument('--port', type=int, default=9088,
                        help='Port to run web services from.')
    return parser.parse_args()


def main() -> None:
    args = default_args()

    app = make_app(args)
    app.listen(args.port)

    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()
