#!/bin/bash

# Evaluation input path.
INPUT_PATH=gs://lotlinxdata/bq-exports/chug/chug_published_feed/csvgz/test/chug_published_feed-test-20200424-000000000000.csv.gz

# Saved output predictions.
OUTPUT_PATH="output.csv"

# Model directory.
MODEL_DIR=gs://lotlinxdata/pricing_description/pricing_description_BASIC_GPU_20200511_154631
# MODEL_DIR=trained_model/

# Format which above model was exported.
EXPORT_FORMAT="CSV"

# Removing previous prediction output.
if [ -f $OUTPUT_PATH ]; then
  echo "Removing output CSV file...\n"
  rm $OUTPUT_PATH
fi


echo "Evalutating..."

python evaluate.py \
  --input-path ${INPUT_PATH} \
  --output-path ${OUTPUT_PATH} \
  --job-dir ${MODEL_DIR} \
  --export-format ${EXPORT_FORMAT}