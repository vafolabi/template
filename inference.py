# import os
from typing import Dict, ForwardRef, Optional, Sequence, Union

from googleapiclient import discovery
from oauth2client.client import GoogleCredentials


NestedArray = Union[
    int, float,
    Sequence[ForwardRef('NestedArray')],
    Dict[str, ForwardRef('NestedArray')],
]

# TODO: Change to GCP project where the AI Platform model is deployed
GCP_PROJECT: str = ''

# TODO: Change to the deployed AI Platform model
MODEL_NAME: str = ''

# If None, the default version will be used
MODEL_VERSION: Optional[str] = None


def predict(instances: Sequence[str]) -> NestedArray:
    """Use a deployed model to AI Platform to perform prediction

    Args:
        instances(List[str, Any]): list of json, csv, or tf.example objects,
            based on the serving function called.

    Returns:
        response - dictionary. If no error, response will include an item
            with 'predictions' key
    """

    credentials = GoogleCredentials.get_application_default()

    service = discovery.build('ml', 'v1', credentials=credentials)
    model_url = f'projects/{GCP_PROJECT}/models/{MODEL_NAME}'

    if MODEL_VERSION is not None:
        model_url += f'/versions/{MODEL_VERSION}'

    request_data = {
        'instances': instances
    }

    response = service.projects().predict(
        body=request_data,
        name=model_url
    ).execute()

    return response
